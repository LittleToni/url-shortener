'use strict'

const express = require('express');
const yup = require('yup');
const { nanoid } = require('nanoid');
const app = express();

const schema = yup.object().shape({
    url: yup.string().trim().url(),
});

app.use(express.json());

app.get('/:id', (req, res) => {
    // Redirect to URL
})

app.post('/url', async (req, res) => {
    const url = req.body.url;
    const slug = nanoid(7).toLowerCase();

    try {
        await schema.validate({
            url,
        })
    } catch (error) {
        next(error);
    }

    res.json({
        url: url,
        slug: slug,
    })
})

app.use((error, req, res, next) => {
    if (error.status) {
        res.status(error.status);
    } else {
        res.status(500);
    }

    res.json({
        message: error.message,
        stack: process.env.NODE_ENV === 'production' ? '' : error.stack
    })
})

const port = process.env.APP_PORT || 5000;

app.listen(port, () => {
    console.log(`Listening on port ${port}!`);
})
