# URL Shortener

Simple self hosted url-shortener API using express.

## Quick Start

- run ```cp .env.example .env``` to define environment
- run ```docker-compose build``` to build the images
- run ```docker-compose up``` to start container
- run ```docker-compose down``` to stop container

## Requirements

- docker
- docker-compose

## Mongo-Express

Visit http://localhost:8081.

## Usage

Example POST request at http://localhost:5000/url to get a short url.

```json
{
  "url": "http://test.example"
}
```
